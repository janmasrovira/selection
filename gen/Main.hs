module Main where

{-
for analysing allocated memory run with
:! time stack exec selection-gen -- +RTS -T
-}

import qualified Benchmarks.Charts.Quickselect as Q
import qualified Benchmarks.Charts.MedianOfMedians as MM
import qualified Benchmarks.Charts.Partitions as PT
import qualified Benchmarks.Charts.Median as M




main :: IO ()
main = do
  --Q.charts1
  --Q.chart2
  --MM.chart1time
  --MM.chart1alloc
  --PT.chart1mem
  --PT.chart1mem
  --PT.chart1
  M.chart1alloc
  M.chart1time
