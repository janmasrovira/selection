module Charts.Simple (
  simplePlot
  , FunSample
  , Coord
  ) where

import Graphics.Rendering.Chart.Backend.Cairo
import Graphics.Rendering.Chart.Easy
--import Graphics.Rendering.Chart.Backend.Diagrams


type Coord = (Double, Double)
type FunSample = [Coord]

t = example

example :: IO ()
example = simplePlot "example.pdf" "Example" [
  ("fun1", [(0, 0), (4,3)])
  , ("fun2", [(0,1), (5,1)])
  , ("fun3", [(x, y) | x <- [0,(0.5)..5], let y = sin x])
  ]


simplePlot :: FilePath -> String -> [(String, FunSample)] -> IO ()
simplePlot path title funs =
  toFile defFileOpts path $ do
  layout_title .= title
  mapM_ (\(name, cs) -> plot (dline name [cs])) funs
  mapM_ (\(_, cs) -> plot (apoint cs)) funs
  layout_legend .= Just myLegendStyle 

myLegendStyle :: LegendStyle
myLegendStyle = def {
  _legend_label_style = myLabStyle
  }
  where myLabStyle = def {
          _font_size = 19
          }
                

defFileOpts :: FileOptions
defFileOpts = def {
  _fo_size = (800,600)
  , _fo_format = PDF
  }


dline :: String -> [[(x0, y0)]] -> EC l2 (PlotLines x0 y0)
dline title values = liftEC $ do
  color <- takeColor
  plot_lines_title .= title
  plot_lines_values .= values
  plot_lines_style . line_width .= 2
  plot_lines_style . line_color .= color


apoint :: [(x0, y0)] -> EC l2 (PlotPoints x0 y0)
apoint values = liftEC $ do
  color <- takeColor
  plot_points_values .= values
  plot_points_style . point_radius .= 3
