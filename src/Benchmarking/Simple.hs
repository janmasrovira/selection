module Benchmarking.Simple (
  analyseTime
  , fromBenchmark
  , benchmarkElems
  , runNmeanOfK
  , meanOfK
  , runNmeanOfKPerm
  , runNmeanOfKPermV
  , runNmeanOfKPermVM
  ) where


import qualified Algorithms.Partitions           as P
import           Algorithms.Randoms
import           Control.DeepSeq
import           Control.Monad.Extra
import           Control.Monad.Primitive
import           Criterion
import           Criterion.Internal
import           Criterion.Main.Options
import           Criterion.Measurement
import           Criterion.Monad
import           Criterion.Report
import           Criterion.Types                 hiding (measure)
import qualified Data.Vector.Unboxed             as U
import           GHC.Int
import           Statistics.Resampling.Bootstrap

b :: Benchmarkable
b = nf (P.partition3 l) piv
  where
    l = [0..n] :: [Int]
    v = U.fromList l
    n = 10^4
    piv = n`div`2



getEstMean :: Report -> Estimate
getEstMean = anMean . reportAnalysis

getEstMeanDR :: DataRecord -> Estimate
getEstMeanDR = getEstMean . fromAnalysed

benchmarkElems :: Benchmark -> [String]
benchmarkElems = r ""
  where
    r :: String -> Benchmark -> [String]
    r pref b =
      case b of
      BenchGroup g bs -> concatMap (r (append pref g)) bs
      Benchmark s b -> [append pref s]
      Environment _ g -> r pref (g undefined)
    append "" x = x
    append s x = s ++ "/" ++ x


fromBenchmark :: Benchmark -> IO [(String, Benchmarkable)]
fromBenchmark = r ""
  where
    r :: String -> Benchmark -> IO [(String, Benchmarkable)]
    r pref b =
      case b of
      BenchGroup g bs -> concatMapM (r (append pref g)) bs
      Benchmark s b -> return [(append pref s, b)]
      Environment x g -> fmap g x >>= r pref
    append "" x = x
    append s x = s ++ "/" ++ x


globalConfig :: Config
globalConfig = defaultConfig{
  timeLimit = 1
  }

fromAnalysed :: DataRecord -> Report
fromAnalysed (Analysed r) = r
fromAnalysed _ = error "not analysed"

-- run a benchmarkable several times for different inputs
analyseTime :: Double -> Benchmarkable -> IO Estimate
analyseTime lim b = withConfig config $ do
  rs <- runAndAnalyseOne 1 "analyzeTime" b
  return $ getEstMeanDR rs
    where
      config = globalConfig {timeLimit = lim}


meanOfK :: Num n => (Measured -> n) -> Int64 -> Benchmarkable -> IO n
meanOfK meas k b = do
  (m, _) <- measure b k
  return $ meas (rescale m)


-- useful when a benchmarkable needs a random environment
-- n = number of different environments generated
-- k = number of iters for each environment
-- total of n*k iters
runNmeanOfK  :: (NFData t)
                => (Measured -> Double) -> Int -> Int64 -> (t -> Benchmarkable) -> IO t -> IO Double
runNmeanOfK meas n k gb g =
  (/fromIntegral n) . sum <$> replicateM n runMeanOfK
    where
      runMeanOfK = do
        x <- g
        return $ rnf x -- ensures that the environment is in normal form
        meanOfK meas k (gb x)


runNmeanOfKPerm :: (Measured -> Double) -> Int -> Double -> Int
                   -> Int64 -> ([Int] -> Benchmarkable) -> IO Double
runNmeanOfKPerm meas m p n k b = runNmeanOfK meas n k b (randPermutation m p)

runNmeanOfKPermV ::
  (Measured -> Double) -> Int -> Double -> Int -> Int64 -> (U.Vector Int -> Benchmarkable)
  -> IO Double
runNmeanOfKPermV meas m p n k b = runNmeanOfK meas n k b (randPermutationV m p)

runNmeanOfKPermVM
  :: (Measured -> Double) -> Int -> Double -> Int -> Int64
  -> (U.MVector RealWorld Int -> Benchmarkable)
  -> IO Double
runNmeanOfKPermVM meas m p n k b = runNmeanOfK meas n k b (randPermutationMV m p)


-- This is ALSO counts the running time required by generating the input!!!
-- runNmeanOfK' :: Int -> Int64 -> IO Benchmarkable -> IO Double
-- runNmeanOfK' n k gb = do
--   ((/fromIntegral n) . sum) <$> replicateM n runMeanOfK
--     where
--       runMeanOfK = gb >>= meanOfK k



report1 :: String -> [(String, Benchmarkable)] -> IO [Estimate]
report1 file funs = withConfig config $ do
  rs <- analyseAll
  report $ map fromAnalysed rs
  return (map getEstMeanDR rs)
  where
    analyseAll = zipWithM (\i (name, b) -> runAndAnalyseOne i name b) [1..] funs
    config = defaultConfig {
      reportFile = Just file
      , verbosity = Verbose
      }

t = report1 "TTTT.txt" [
  ("partition3", b)
  , ("part3", b)
  ]
