module Benchmarks.Types where

import           Control.Arrow
import           Control.Monad.ST
import qualified Data.Vector.Unboxed         as U
import qualified Data.Vector.Unboxed.Mutable as UM

type MedianEnv = ([Int], U.Vector Int, UM.MVector RealWorld Int)

mapplySecond :: [((d, a -> c), a)] -> [(d, c)]
mapplySecond = map applySecond

applySecond :: ((d, a -> c), a) -> (d, c)
applySecond = (\(f, lv) -> second ($lv) f)

