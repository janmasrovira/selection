{-# LANGUAGE TupleSections #-}
module Benchmarks.Partitions (
  newPartitionsEnv
  , partitions
  , partitionsB
  , PartitionsEnv
  , runPtList
  , runPtVector
  , runPtMVector
  , runPtList'
  ) where


import qualified Algorithms.Partitions       as P
import           Algorithms.Randoms
import           Algorithms.Shuffle
import           Benchmarking.Simple
import           Benchmarks.Types
import           Control.Arrow
import           Control.Monad.Primitive
import           Control.Monad.Random
import           Criterion
import           Criterion.Types
import qualified Data.Vector.Unboxed         as U
import qualified Data.Vector.Unboxed.Mutable as UM
import           GHC.Int

type PartitionsEnv = ([Int], U.Vector Int, UM.MVector RealWorld Int, Int)

withPiv :: MonadRandom m => (Int -> Double -> m a) -> Int -> Double -> m (a, Int)
withPiv rp m p = do
  l <- rp m p
  (l,) <$> getRandomR (1, m)

runPtList :: (Measured -> Double) -> Int -> Double -> Int -> Int64 -> IO Double
runPtList meas m p n k = runNmeanOfK meas n k (uncurry (snd ptList))
                    (withPiv randPermutation m p)

runPtList' :: (Measured -> Double) -> Int -> Double -> Int -> Int64 -> IO Double
runPtList' meas m p n k = runNmeanOfK meas n k (uncurry (snd ptList'))
                          (withPiv randPermutation m p)


runPtVector :: (Measured -> Double) -> Int -> Double -> Int -> Int64 -> IO Double
runPtVector meas m p n k = runNmeanOfK meas n k (uncurry (snd ptVector))
                      (withPiv randPermutationV m p)

runPtMVector :: (Measured -> Double) -> Int -> Double -> Int -> Int64 -> IO Double
runPtMVector meas m p n k = runNmeanOfK meas n k (uncurry (snd ptMVector))
                       (withPiv randPermutationMV m p)


ptList :: (String, [Int] -> Int -> Benchmarkable)
ptList = ("list", \l piv -> nf (P.partition3 l) piv)

ptList' :: (String, [Int] -> Int -> Benchmarkable)
ptList' = ("list'", \l piv -> nf (P.partition3' l) piv)

ptVector :: (String, U.Vector Int -> Int -> Benchmarkable)
ptVector = ("vector", \v piv -> nf (P.partition3V v) piv)

ptMVector :: (String, UM.MVector RealWorld Int -> Int -> Benchmarkable)
ptMVector = ("mutable vector", \mv piv -> nfIO (P.partition3VM mv piv))

                     
newPartitionsEnv :: Int -> Double -> IO PartitionsEnv
newPartitionsEnv n p = do
  l <- shuffleP p [1..n]
  let v = U.fromList l
  mv <- U.thaw v
  piv <- getRandomR (1, n)
  return (l, v, mv, piv)


partitions :: PartitionsEnv -> Benchmark
partitions penv = 
  bgroup "Partitions" (map (uncurry bench) (partitionsB penv))


partitionsB :: PartitionsEnv -> [(String, Benchmarkable)]
partitionsB ~(l, v, mv, piv) =
  mapplySecond [(second uncurry ptList, (l, piv))
               ,(second uncurry ptList', (l, piv))]
  ++ mapplySecond [(second uncurry ptVector, (v, piv))]
  ++ mapplySecond [(second uncurry ptMVector, (mv, piv))]
