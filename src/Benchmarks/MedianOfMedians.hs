module Benchmarks.MedianOfMedians where

import qualified Algorithms.MedianOfMedians  as MM
import           Benchmarking.Simple
import           Benchmarks.Types
import           Control.Monad.ST
import           Criterion
import           Criterion.Types
import qualified Data.Vector.Unboxed         as U
import qualified Data.Vector.Unboxed.Mutable as UM
import           GHC.Int

runMmList :: (Measured -> Double) -> Int -> Double -> Int -> Int64 -> IO Double
runMmList meas m p n k = runNmeanOfKPerm meas m p n k (snd mmList)

runMmVector :: (Measured -> Double) -> Int -> Double -> Int -> Int64 -> IO Double
runMmVector meas m p n k = runNmeanOfKPermV meas m p n k (snd mmVector)

runMmMVector :: (Measured -> Double) -> Int -> Double -> Int -> Int64 -> IO Double
runMmMVector meas m p n k = runNmeanOfKPermVM meas m p n k (snd mmMVector)


mmList :: (String, [Int] -> Benchmarkable)
mmList = ("list", nf MM.median)

mmVector :: (String, U.Vector Int -> Benchmarkable)
mmVector = ("vector", nf MM.medianV)

mmMVector :: (String, UM.MVector RealWorld Int -> Benchmarkable)
mmMVector = ("mutable vector", nfIO . MM.medianVM)


medianOfMedians :: MedianEnv ->  Benchmark
medianOfMedians menv =
  bgroup "Median of Medians" $ map (uncurry bench) (medianOfMediansB menv)


medianOfMediansB :: MedianEnv -> [(String, Benchmarkable)]
medianOfMediansB ~(l, v, mv) =
  mapplySecond [(mmList, l)]
  ++
  mapplySecond [(mmVector, v)]
  ++
  mapplySecond [(mmMVector, mv)]
