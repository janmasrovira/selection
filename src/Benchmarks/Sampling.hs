module Benchmarks.Sampling (
  sampling
  , newSamplingEnv
  , samplingB
  , SamplingEnv
  ) where


import           Criterion

import           Algorithms.Sampling

import           Control.Monad.ST
import qualified Data.Vector.Unboxed         as U
import qualified Data.Vector.Unboxed.Mutable as UM



type SamplingEnv = ([Int], U.Vector Int, UM.MVector RealWorld Int, Int)


newSamplingEnv :: Int -> IO SamplingEnv
newSamplingEnv n = do
  mv <- U.thaw v
  return (l, v, mv, k)
  where
    l = [1..n] :: [Int]
    v = U.fromList l
    k = max 1 (n `div` 20)


sampling :: SamplingEnv -> Benchmark
sampling senv = bgroup "Sampling" (map (uncurry bench) (samplingB senv))


samplingB :: SamplingEnv -> [(String, Benchmarkable)]
samplingB ~(l, v, mv, k) = [
  ("reservoir" , nfIO (sample k l))
  , ("reservoir vector", nfIO (sampleV k mv))
  , ("indexed (with replacement)", nfIO (sampleRV k v))
  ] 
