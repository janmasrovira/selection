{-# LANGUAGE TupleSections #-}
module Benchmarks.Charts.Quickselect (
  charts1
  , chart2
  ) where

import Charts.Simple

import Benchmarks.Quickselect
import Control.Monad
import Criterion.Types

toChartFolder = ("charts/qs/" ++)
toDataFolder = ("data/qs/" ++)

charts1 = charts1gen measTime
chart2 = chart2gen measTime

charts1gen :: (Measured -> Double) -> IO ()
charts1gen meas = do
  measures <- forM ps $ \p -> do
    putStrLn $ "p = " ++ show p
    forM [("list", runQsList), ("list rand", runQsListRand)
         , ("vector", runQsVector), ("vector rand", runQsVectorRand)] $ \(name, f) -> do
      putStrLn name
      fmap (name,) <$> forM [100, 200 .. 1500] $ \m ->
        (fromIntegral m ::Double,) <$> f meas m p n k
  writeFile (toDataFolder "charts1.txt") (show measures)
  charts1plot measures
    where
      n = 10
      k = 5

ps = [0,0.25,0.5,0.75,1]


charts1plot measures = forM_ (zip ps measures) $ \(p, meas) ->
  simplePlot (toChartFolder "charts1_p" ++ show p ++ ".pdf")
  ("Quickselect - Running time, p = " ++ show p) meas

charts1plotFiles = readFile (toDataFolder "charts1.txt") >>=
                 charts1plot . read
                 

chart2gen :: (Measured -> Double) -> IO ()
chart2gen meas = do
  measure <- forM [ ("list rand", runQsListRand)
                   ,("vector rand", runQsVectorRand)] $ \(name, f) -> do
      putStrLn name
      fmap (name,) <$> forM [100, 200 .. 8000] $ \m -> do
        print m
        (fromIntegral m ::Double,) <$> f meas m p n k
  writeFile (toDataFolder "charts2.txt") (show measure)
  simplePlot (toChartFolder "charts2.pdf")
    ("Quickselect, p = " ++ show p) measure
    where
      n = 10
      k = 5
      p = 1
