{-# LANGUAGE TupleSections #-}
module Benchmarks.Charts.Median (
  chart1time
  , chart1alloc
  ) where


import Benchmarks.MedianOfMedians
import Benchmarks.Quickselect
import Benchmarks.NaiveMedian
import Charts.Simple
import Control.Monad
import Benchmarks.RandomizedMedian
import Criterion.Types

toChartFolder = ("charts/med/" ++)
toDataFolder = ("data/med/" ++)


chart1time = chart1gen "Median - Running time" "-time" measTime
chart1alloc = chart1gen "Median - Allocated bytes" "-alloc" (fromIntegral . measAllocated)

chart1gen :: String -> String -> (Measured -> Double) -> IO ()
chart1gen title lab meas = do
  measure <- forM [ ("MM/list", runMmList)
                  , ("QS/vector rand", runQsVectorRand)
                  , ("Sort", runSortList)
                  , ("RandMed", runRmVector)] $ \(name, f) -> do
    print name
    fmap (name,) <$> forM ns $ \m -> do
        print m
        (fromIntegral m ::Double,) <$> f meas m p n k
  writeFile (toDataFolder $ fname ++ ".txt") (show measure)
  chart1plot title lab measure
    where
      n = 10
      k = 10
      p = 1
      ns = [100, 5000 .. 180000]
      fname = "chart1" ++ lab


chart1plot title lab = simplePlot (toChartFolder $ fname ++ ".pdf") title
  where
    fname = "chart1" ++ lab


chart1plotFile = readFile (toDataFolder "chart1.txt")
                 >>= chart1plot "Median - Running time" "-time" . read
