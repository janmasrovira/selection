{-# LANGUAGE TupleSections #-}

module Benchmarks.Charts.Partitions (
  chart1
  , chart1mem
  ) where


import Charts.Simple

import Benchmarks.Partitions
import Control.Monad
import Criterion.Types

toChartFolder = ("charts/pt/" ++)
toDataFolder = ("data/pt/" ++)

chart1 :: IO ()
chart1 = chart1gen "Partitions - Running time" "-time" measTime

chart1mem :: IO ()
chart1mem = chart1gen "Partitions - Allocated Bytes" "-alloc" (fromIntegral . measAllocated)

chart1gen :: String -> String -> (Measured -> Double) -> IO ()
chart1gen title lab meas = do
  measure <- forM [ ("list ", runPtList)
                  , ("list'", runPtList')
                  , ("vector", runPtVector)
                  , ("mutable vector", runPtMVector)] $ \(name, f) -> do
      putStrLn name
      fmap (name,) <$> forM [100, 1100 .. 35000] $ \m -> do
        print m
        (fromIntegral m ::Double,) <$> f meas m p n k
  writeFile (toDataFolder $ fname ++ ".txt") (show measure)
  chart1plotFile title lab measure
    where
      n = 10
      k = 10
      p = 1
      fname = "charts1" ++ lab

chart1plotFile title lab = simplePlot (toChartFolder $ fname ++ ".pdf") title
  where
    fname = "charts1" ++ lab

plotTime = readFile (toDataFolder "charts1.txt")
           >>= chart1plotFile "Partitions - Running time" "-time" . read
