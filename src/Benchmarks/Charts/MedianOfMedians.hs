{-# LANGUAGE TupleSections #-}

module Benchmarks.Charts.MedianOfMedians (
  chart1time
  , chart1alloc
  ) where

import Benchmarks.MedianOfMedians
import Charts.Simple
import Control.Monad
import Criterion.Types

toChartFolder = ("charts/mm/" ++)
toDataFolder = ("data/mm/" ++)

chart1time = chart1gen "Median Of Medians - Running time" "-time" measTime
chart1alloc = chart1gen "Median Of Medians - Allocated bytes" "-alloc" (fromIntegral . measAllocated)



chart1gen :: String -> String -> (Measured -> Double) -> IO ()
chart1gen title lab meas = do
  measure <- forM [ ("list", runMmList)
                   , ("vector", runMmVector)
                   , ("mutable vector", runMmMVector)] $ \(name, f) -> do
      fmap (name,) <$> forM [100, 600 .. 24000] $ \m -> do
        print m
        (fromIntegral m ::Double,) <$> f meas m p n k
  writeFile (toDataFolder $ fname ++ lab ++ ".txt") (show measure)
  chart1plotFile title lab measure
    where
      n = 10
      k = 10
      p = 1
      fname = "chart1"


chart1plotFile title lab = simplePlot (toChartFolder $ fname ++ ".pdf") title
  where
    fname = "chart1" ++ lab

plotTime = readFile (toDataFolder "chart1.txt")
           >>= chart1plotFile "Median Of Medians - Running time" "-time" . read
