module Benchmarks.Median (
  newMedianEnv
  , genMedianEnv
  , randomMedian
  , randomMedianB
  , medianOfMediansB
  , medianOfMedians
  , sortingMedian
  , sortingMedianB
  , quickselect
  , quickselectB
  , median
  , medianB
  , MedianEnv
  ) where

import           Algorithms.Shuffle
import           Benchmarks.MedianOfMedians
import           Benchmarks.NaiveMedian
import           Benchmarks.Quickselect
import           Benchmarks.RandomizedMedian
import           Benchmarks.Types
import           Control.Monad.Random
import           Control.Monad.ST
import           Criterion
import qualified Data.Vector.Unboxed         as U


genMedianEnv :: Int -> Double -> StdGen -> IO MedianEnv
genMedianEnv n p gen = do
  mv <- U.thaw v
  return (l, v, mv)
  where
    l = runST $ evalRandT (shuffleP p [0..n]) gen :: [Int]
    v = U.fromList l


newMedianEnv :: Int -> Double -> IO MedianEnv
newMedianEnv n p = do
  l <- shuffleP p [1..n]
  let v = U.fromList l
  mv <- U.thaw v
  return (l, v, mv)


 
median :: IO MedianEnv -> Benchmark
median genMedEnv = bgroup "Median" $ map (env genMedEnv) [
  randomMedian
  , medianOfMedians
  , quickselect
  , sortingMedian
  ]

medianB :: MedianEnv -> [(String, Benchmarkable)]
medianB menv = concatMap ($menv) [
  randomMedianB
  , medianOfMediansB
  , quickselectB
  , sortingMedianB]
