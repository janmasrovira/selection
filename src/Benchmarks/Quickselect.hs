module Benchmarks.Quickselect where

import qualified Algorithms.Quickselect as Q
import           Benchmarking.Simple
import           Benchmarks.Types
import           Criterion
import           Criterion.Types
import qualified Data.Vector.Unboxed    as U
import           GHC.Int

  
runQsList :: (Measured -> Double) -> Int -> Double -> Int -> Int64 -> IO Double
runQsList meas m p n k = runNmeanOfKPerm meas m p n k (snd qsList)

runQsListRand :: (Measured -> Double) -> Int -> Double -> Int -> Int64 -> IO Double
runQsListRand meas m p n k = runNmeanOfKPerm meas m p n k (snd qsListRand)

runQsVector :: (Measured -> Double) -> Int -> Double -> Int -> Int64 -> IO Double
runQsVector meas m p n k = runNmeanOfKPermV meas m p n k (snd qsVector)

runQsVectorRand :: (Measured -> Double) -> Int -> Double -> Int -> Int64 -> IO Double
runQsVectorRand meas m p n k = runNmeanOfKPermV meas m p n k (snd qsVectorRand)

                    
qsList :: (String, [Int] -> Benchmarkable)
qsList = ("list", nf Q.median)

qsListRand :: (String, [Int] -> Benchmarkable)
qsListRand = ("list rand", nfIO  . Q.medianR)

qsVector :: (String, U.Vector Int -> Benchmarkable)
qsVector = ("vector", nf Q.medianV)

qsVectorRand :: (String, U.Vector Int -> Benchmarkable)
qsVectorRand = ("vector rand", nfIO . Q.medianRV)


quickselect :: MedianEnv -> Benchmark
quickselect menv = 
  bgroup "Quickselect" $ map (uncurry bench) (quickselectB menv)

  
quickselectB :: MedianEnv -> [(String, Benchmarkable)]
quickselectB ~(l, v, _) = 
  mapplySecond [ (qsList, l)
  , (qsListRand, l) ]
  ++
  mapplySecond [ (qsVector, v)
  , (qsVectorRand, v)]
  where
