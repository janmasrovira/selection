module Benchmarks.NaiveMedian where

import qualified Algorithms.NaiveMedian as NM
import           Benchmarking.Simple
import           Benchmarks.Types
import           Criterion
import           Criterion.Types
import           GHC.Int

runSortList :: (Measured -> Double) -> Int -> Double -> Int -> Int64 -> IO Double
runSortList meas m p n k = runNmeanOfKPerm meas m p n k (snd sortBIF)


sortBIF :: (String, [Int] -> Benchmarkable)
sortBIF = ("built in", nf NM.median)

sortingMedianB :: MedianEnv -> [(String, Benchmarkable)]
sortingMedianB ~(l, _, _) = mapplySecond [(sortBIF, l)]               

sortingMedian :: MedianEnv -> Benchmark
sortingMedian menv =
  bgroup "Sorting" $ map (uncurry bench) (sortingMedianB menv)
