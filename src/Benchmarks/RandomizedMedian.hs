module Benchmarks.RandomizedMedian where

import qualified Algorithms.RandomizedMedian as RM
import           Benchmarking.Simple
import           Benchmarks.Types
import           Criterion
import           Criterion.Types
import qualified Data.Vector.Unboxed         as U
import           GHC.Int


runRmVector :: (Measured -> Double) -> Int -> Double -> Int -> Int64 -> IO Double
runRmVector meas m p n k = runNmeanOfKPermV meas m p n k (snd rmVector)

rmVector :: (String, U.Vector Int -> Benchmarkable) 
rmVector =  ("vector", nfIO . RM.medianV)

randomMedian :: MedianEnv -> Benchmark
randomMedian menv = bgroup "Randomized Median" $ map (uncurry bench) (randomMedianB menv)

randomMedianB :: MedianEnv -> [(String, Benchmarkable)]
randomMedianB ~(_, v, _) = mapplySecond [
  (rmVector, v)
  ]
