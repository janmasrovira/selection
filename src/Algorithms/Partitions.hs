module Algorithms.Partitions (
  partition3
  , partition3V
  , unsafePartition3V
  , partition3VM
  , partition3'
  ) where


import           Algorithms.Shuffle
import           Control.Monad
import           Control.Monad.Loops         as L
import           Control.Monad.Primitive
import           Data.List                   (foldl')
import           Data.Maybe
import qualified Data.Vector.Generic         as V
import qualified Data.Vector.Unboxed         as U
import           Data.Vector.Unboxed.Mutable (Unbox)
import qualified Data.Vector.Unboxed.Mutable as UM
import qualified Other.MutableVector         as MV


partition3 :: Ord t => [t] -> t -> ([t], Int, [t])
partition3 [] _ = ([], 0, [])
partition3 (h:t) pivot =
  case compare h pivot of
   LT -> (h:l, e, g)
   EQ -> (l, 1 + e, g)
   GT -> (l, e, h:g)
  where (l, e, g) = partition3 t pivot


partition3' :: Ord t => [t] -> t -> ([t], Int, [t])
partition3' l pivot = foldl' f ([], 0, []) l
  where f (l, e, g) h =
          case compare h pivot of
          LT -> (h:l, e, g)
          EQ -> (l, 1 + e, g)
          GT -> (l, e, h:g)


partition3V :: (Ord a, U.Unbox a) => U.Vector a -> a -> (U.Vector a, Int, U.Vector a)
partition3V vec pivot = (l, nle - nl, g)
  where
    (le, g) = U.unstablePartition (<= pivot) vec
    nle = V.length le
    l = V.filter (< pivot) le
    nl = V.length l


unsafePartition3V :: (Ord a, U.Unbox a, PrimMonad m)
                     => U.Vector a -> a -> m (U.Vector a, Int, U.Vector a)
unsafePartition3V vec pivot = do
  v <- U.unsafeThaw vec
  (l, e, r) <- partition3VM v pivot
  l' <- U.unsafeFreeze l
  r' <- U.unsafeFreeze r
  return (l', e, r')



-- CONDITION: the vector MUST contain the pivot
-- fast 3-way partition
partition3VM :: (Ord a, PrimMonad m, Unbox a)
                => UM.MVector (PrimState m) a -> a
                -> m (UM.MVector (PrimState m) a, Int, UM.MVector (PrimState m) a)
partition3VM v piv = do
  pivix <- fromJust <$> firstM (\ix -> (==piv) <$> UM.unsafeRead v ix) [0..n - 1]
  UM.unsafeSwap v pivix 0
  step 1 1 (n - 1) (n - 1) v
  where
    n = UM.length v
    step p i j q v
      | i <= j = do
          i' <- walki
          j' <- walkj
          if i' > j'
            then
            return (slice v p j', p + (n - q) - 1, slice v i' q)
            else do
            UM.unsafeSwap v i' j' 
            ai <- UM.unsafeRead v i'
            when (ai == piv) (UM.unsafeSwap v i' p)
            let p' = if ai == piv then p + 1 else p
            aj <- UM.unsafeRead v j'
            when (aj == piv) (UM.unsafeSwap v j' q)
            let q' = if aj == piv then q - 1 else q
            step p' (max i' p') (min j' q') q' v
      | otherwise = return (slice v p j, p + (n - q) - 1, slice v i q)
      where
        walki = fromMaybe n <$> firstM (\ix -> (>= piv) <$> UM.unsafeRead v ix) [i..n - 1]
        walkj = fromMaybe (-1) <$> firstM (\ix -> (piv >=) <$> UM.unsafeRead v ix) [j, j - 1..1]
        slice v i j = let len = max 0 (j - i + 1) in UM.unsafeSlice i len v 


f :: IO ()
f = do
  l <- MV.fromList [1..4::Int]
  let v = UM.slice 0 (-1) l
  MV.toList v >>= print

t :: IO ()
t = do
  v <- shuffle (take 15 $ cycle [1..4::Int]) >>= MV.fromList
  v <- MV.fromList [1..4::Int] 
  MV.toList v >>= print
  piv <- UM.read v 0--(UM.length v `div` 2)
  putStrLn ("Pivot = " ++ show piv)
  (l, e, r) <- partition3VM v piv
  l' <- MV.toList l
  r' <- MV.toList r
  print $ (l', e, r')
