module Algorithms.RandomizedMedian (
  medianV
  ) where

import           Algorithms.Sampling
import           Algorithms.Shuffle
import           Control.Monad
import           Control.Monad.Random
import           Data.List            (sort)
import qualified Data.Vector.Unboxed  as U

medianVM :: (Ord a, U.Unbox a, MonadRandom m) => U.Vector a -> m (Maybe a)
medianVM v = do
  r <- sort <$!> sampleRV rn v
  let d = r !! ixd
      u = r !! ixu
      (c, ld, lu) = f d u v
      ixm = max 0 $ ((n - 1)`div`2) - ld
      lc = length c
      res
        | ld > (n`div`2) || lu > (n`div`2)
          || lc > (floor $ 4*n34)
          || ixm >= lc = Nothing
        | otherwise = Just (sort c !! ixm) 
  return res
    where
      f d u s = U.foldl' f' ([], 0, 0) s 
        where
          f' (c', ld', lu') x
            | d <= x, x <= u = (x:c', ld', lu')
            | x < d = (c', 1 + ld', lu')
            | otherwise = (c', ld', 1 + lu')
      ixd, ixu :: Int
      ixd = max 0 $ truncate (1/2*n34 - sqrt nd)
      ixu = min (rn - 1) $ truncate (n34/2 + sqrt nd)
      nd, n34 :: Double
      nd = fromIntegral n
      n34 = nd ** (3/4)
      rn :: Int
      rn = ceiling n34
      n = U.length v



medianV :: (Ord a, U.Unbox a, MonadRandom m) => U.Vector a -> m a
medianV v = do
  x <- medianVM v 
  case x of
    Just m -> return m
    _ -> medianV v


t n = truncate (1/2*n34 + sqrt nd)
  where
    nd, n34 :: Double
    nd = fromIntegral n
    n34 = nd ** (3/4)

doUntilFail :: Int -> IO ()
doUntilFail n = U.fromList <$> shuffle [0..n] >>= untilFail
                >>= \x -> putStrLn ("iterations until fail: " ++ show x)

-- returns the number of iterations that succeeded until it failed
untilFail :: (Ord a, U.Unbox a, Show a) => U.Vector a -> IO Int
untilFail v = f 0
  where f x = do
          when ((x `mod` 100) == 0) (print x)
          res <- medianVM v
          case res of
            Nothing -> return x
            Just _ -> f (x + 1)
