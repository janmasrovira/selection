module Algorithms.Quickselect
    ( median
    , medianR
    , medianV
    , medianRV
    ) where


import           Algorithms.Partitions hiding (partition3)
import           Control.Monad.Random
import qualified Data.Vector.Unboxed   as U

select :: Ord a => [a] -> Int -> a
select v k
  | nle < k = select r (k - nle)
  | nl < k = pivot
  | otherwise = select l k
  where
    pivot = head v
    (l, e, r) = partition3' v pivot
    nl = length l
    nle = e + nl


selectR :: (Ord r, MonadRandom m) => [r] -> Int -> m r
selectR v k = do
  pivot <- uniform v
  selectR' pivot
    where
      selectR' pivot
        | nle < k = selectR r (k - nle)
        | nl < k = return pivot
        | otherwise = selectR l k
        where
          (l, e, r) = partition3' v pivot
          nl = length l
          nle = e + nl

uniformV :: (U.Unbox a, MonadRandom m) => U.Vector a -> m a
uniformV v = U.unsafeIndex v <$> getRandomR (0, U.length v - 1)


selectV :: (Ord r, U.Unbox r) => U.Vector r -> Int -> r
selectV v k =
  selectR' (U.unsafeHead v)
  where
    selectR' pivot
      | nle < k = selectV r (k - nle)
      | nl < k = pivot
      | otherwise = selectV l k
      where
        (l, e, r) = partition3V v pivot
        nl = U.length l
        nle = e + nl


selectRV :: (Ord r, U.Unbox r, MonadRandom m) => U.Vector r -> Int -> m r
selectRV v k = do
  pivot <- uniformV v
  selectR' pivot
    where
      selectR' pivot
        | nle < k = selectRV r (k - nle)
        | nl < k = return pivot
        | otherwise = selectRV l k
        where
          (l, e, r) = partition3V v pivot
          nl = U.length l
          nle = e + nl


-- returns the (n/2)-th smallest element in v (the median)
median :: Ord a => [a] -> a
median l = select l ((length l `div` 2))

medianR :: (MonadRandom m, Ord a)
           => [a] -> m a
medianR l = selectR l ((length l `div` 2))


medianV :: (Ord a, U.Unbox a) => U.Vector a -> a
medianV l = selectV l ((U.length l `div` 2))

medianRV :: (Ord a, U.Unbox a, MonadRandom m) => U.Vector a -> m a
medianRV l = selectRV l ((U.length l `div` 2))
