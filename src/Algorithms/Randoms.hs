module Algorithms.Randoms where


import           Algorithms.Shuffle
import           Control.Monad.Primitive
import           Control.Monad.Random        as R
import qualified Data.Vector.Unboxed.Mutable as UM
import qualified Other.MutableVector         as MV
import           Other.RandPrimMonad         ()
import qualified Data.Vector.Unboxed as V


randPermutation:: (PrimMonad m, MonadRandom m)
                  => Int -> Double -> m [Int]
randPermutation n p = shuffleP p [1..n]


randPermutationV :: (PrimMonad m, MonadRandom m)
                    => Int -> Double -> m (V.Vector Int)
randPermutationV n p = V.fromList <$> randPermutation n p


randPermutationMV :: (PrimMonad m, MonadRandom m)
                    => Int -> Double -> m (UM.MVector (PrimState m) Int)
randPermutationMV n p = randPermutation n p >>= MV.fromList
