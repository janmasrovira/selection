module Algorithms.NaiveMedian (
  median
  ) where

import Data.List (sort)

median :: Ord a => [a] -> a
median l = sort l!!((length l - 1) `div` 2)
