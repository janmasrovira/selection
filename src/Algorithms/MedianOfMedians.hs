module Algorithms.MedianOfMedians
       ( median
       , medianV
       , medianV'
       , medianVM
       ) where

import           Algorithms.Partitions            hiding (partition3)
import           Algorithms.Shuffle
import           Control.Monad.Primitive
import           Data.List                        (sort)
import qualified Data.Vector.Algorithms.Insertion as I
import qualified Data.Vector.Unboxed              as U
import qualified Data.Vector.Unboxed.Mutable      as UM
import qualified Other.MutableVector              as MV


-- returns the k-th smallest element in v
select :: Ord a => [a] -> Int -> a
select [x] _ = x
select v k
  | nle < k = select r (k - nle)
  | nl < k = pivot
  | otherwise = select l k
  where
    pivot = medianOfMedians v
    (l, e, r) = partition3' v pivot
    nl = length l
    nle = e + nl

selectV :: (U.Unbox a, Ord a) => U.Vector a -> Int -> a
selectV v k
  | U.length v == 1 = U.unsafeHead v
  | nle < k = selectV r (k - nle)
  | nl < k = pivot
  | otherwise = selectV l k
  where
    pivot = medianOfMediansV v
    (l, e, r) = partition3V v pivot
    nl = U.length l
    nle = e + nl


selectV' :: (Ord a, U.Unbox a, PrimMonad m)
            => U.Vector a -> Int -> m a
selectV' v k
  | U.length v == 1 = return (U.unsafeHead v)
  | otherwise = do
      pivot <- medianOfMediansV' v
      (l, e, r) <- unsafePartition3V v pivot
      let nl = U.length l
          nle = e + nl
          ret
            | nle < k = selectV' r (k - nle)
            | nl < k = return pivot
            | otherwise = selectV' l k
      ret


selectVM :: (Ord a, U.Unbox a, PrimMonad m)
            => UM.MVector (PrimState m) a -> Int -> m a
selectVM v k
  | UM.length v == 1 = UM.unsafeRead v 0
  | otherwise = do
      pivot <- medianOfMediansVM v
      (l, e, r) <- partition3VM v pivot
      let nl = UM.length l
          nle = e + nl
          ret
            | nle < k = selectVM r (k - nle)
            | nl < k = return pivot
            | otherwise = selectVM l k
      ret


-- returns the (n/2)-th smallest element in v (the median)
median :: Ord a => [a] -> a
median l = select l (length l `div` 2)


medianOfMedians :: Ord a => [a] -> a
medianOfMedians = median . map median5 . chunksOf5

medianOfMediansV :: (Ord a, U.Unbox a) => U.Vector a -> a
medianOfMediansV = medianV . U.fromList . map median5V . chunksOf5V


medianOfMediansV' :: (Ord a, U.Unbox a, PrimMonad m) => U.Vector a -> m a
medianOfMediansV' = medianV' . U.fromList . map median5V . chunksOf5V


medianVM :: (Ord a, U.Unbox a, PrimMonad m)
            => UM.MVector (PrimState m) a -> m a
medianVM v = selectVM v ((UM.length v `div` 2))


t :: IO ()
t = do
  let l = [1,4,1,2,3,3,4,1,2,4,2,1,3,2,3] :: [Int]
  v <- --return l
       shuffle (take 1050 $ cycle [1..4::Int])
       >>= MV.fromList
  --MV.toList v >>= print
  m <- medianVM v
  let m1 = medianV (U.fromList l)
  m2 <- medianV' (U.fromList l)
  print (m2 == m1)




-- sorts each chunk of the vector
medianOfMediansVM :: (Ord a, U.Unbox a, PrimMonad m)
                    => UM.MVector (PrimState m) a -> m a
medianOfMediansVM v = do
  mapM (median5' v) [0, 5 .. n - 1] >>= MV.fromList >>= medianVM
  where
    sort5 v i = I.sortByBounds compare v i (safeU $ i + 5)
    median5' v i = let j = safeU (i + 5)
                  in sort5 v i >> UM.read v ((i + j) `div` 2)
    n = UM.length v
    safeU = min (n - 1)


medianV :: (Ord a, U.Unbox a) => U.Vector a -> a
medianV v = selectV v (U.length v `div` 2)

medianV' :: (Ord a, U.Unbox a, PrimMonad m) => U.Vector a -> m a
medianV' v = selectV' v (U.length v `div` 2)


median5V :: (Ord a, U.Unbox a) => U.Vector a -> a
median5V = median5 . U.toList

median5 :: Ord a => [a] -> a
median5 l = sort l !! (length l `div` 2)


chunksOf :: Int -> [t] -> [[t]]
chunksOf _ [] = []
chunksOf k v = l : chunksOf k r
  where (l, r) = splitAt k v


chunksOf5 :: [e] -> [[e]]
chunksOf5 = chunksOf 5


chunksOfV :: U.Unbox a => Int -> U.Vector a -> [U.Vector a]
chunksOfV k v
  | U.null v = []
  | otherwise = l : chunksOfV k r
  where (l, r) = U.splitAt k v


chunksOf5V :: U.Unbox e => U.Vector e -> [U.Vector e]
chunksOf5V = chunksOfV 5
