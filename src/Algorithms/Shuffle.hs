module Algorithms.Shuffle (
  shuffleP, shuffle
  ) where

import           Control.Monad
import           Control.Monad.Primitive
import           Control.Monad.Random        as R
import           Data.Vector.Unboxed.Mutable (Unbox)
import qualified Data.Vector.Unboxed.Mutable as UM
import qualified Other.MutableVector         as MV
import           Other.RandPrimMonad         ()
import           Control.Monad.ST



coin :: MonadRandom m => Double -> m Bool
coin p = (<p) <$> getRandomR (0, 1)


shuffle :: (Unbox a, PrimMonad m, MonadRandom m)
            => [a] -> m [a]
shuffle = shuffleP 1


shuffleP :: (Unbox a, PrimMonad m, MonadRandom m)
            => Double -> [a] -> m [a]
shuffleP p l = do
  mv <- MV.fromList l
  mapM_ (rswap mv) [0..UM.length mv - 1]
  MV.toList mv
  where
    rswap v i =
      let n = UM.length v
      in do
        b <- coin p
        when b $ getRandomR (i, n - 1) >>= UM.unsafeSwap v i

pureExample :: [Int] -> [Int]
pureExample l = runST $ evalRandT (shuffle l) (mkStdGen 123)
