module Algorithms.Sampling
       (sample
       , sampleV
       , sampleRV
       ) where


import           Other.RandPrimMonad    ()
import           Control.Monad
import           Control.Monad.Primitive
import           Control.Monad.Random
import           Control.Monad.ST
import qualified Data.Vector.Unboxed         as U
import           Data.Vector.Unboxed.Mutable (Unbox)
import qualified Data.Vector.Unboxed.Mutable as UM


-- no replacement
-- does not shuffle
-- note that if k == n then the elements are returned in the same order
-- O(n)
sample :: (PrimMonad m, MonadRandom m, Unbox a)
          => Int -> [a] -> m [a]
sample k l = do
  v <- U.unsafeThaw (U.fromList h)
  f v (zip [k..] t)
  where
    (h, t) = splitAt k l
    f r [] = U.toList <$> U.unsafeFreeze r
    f r ((i, x):xs) = do
      j <- getRandomR (1, i)
      when (j <= k) (UM.unsafeWrite r (j - 1) x)
      f r xs


-- the vector is modified!
-- no replacement
-- O(k)
sampleV :: (PrimMonad m, MonadRandom m, Unbox a)
           => Int -> UM.MVector (PrimState m) a -> m [a]
sampleV k v = mapM_ f ixs >> mapM (UM.unsafeRead v) ixs
  where
    ixs = [0..k - 1]
    f i = getRandomR (i, n - 1) >>= UM.unsafeSwap v i 
    n = UM.length v


-- with replacement
-- vector not modified
-- O(k)
sampleRV :: (MonadRandom f, Unbox a)
            => Int -> U.Vector a -> f [a]
sampleRV k v = map (U.unsafeIndex v) <$> replicateM k (getRandomR (0, n - 1))
  where
    n = U.length v


pureExample :: [Int]
pureExample = runST $ evalRandT (sample 10 [1..100]) (mkStdGen 123)

ioExample :: IO [Int]
ioExample = sample 10 [1..100]
