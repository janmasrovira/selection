module Other.MutableVector (
  fromList
  , toList
  , unsafeFromList
  , unsafeToList
  ) where

import           Control.Monad.Primitive
import qualified Data.Vector.Unboxed         as U
import           Data.Vector.Unboxed.Mutable (Unbox)
import qualified Data.Vector.Unboxed.Mutable as UM


fromList :: (Unbox a, PrimMonad m) => [a] -> m (UM.MVector (PrimState m) a)
fromList = U.thaw . U.fromList

toList :: (Unbox a, PrimMonad m) => UM.MVector (PrimState m) a -> m [a]
toList = fmap U.toList . U.freeze

unsafeFromList :: (Unbox a, PrimMonad m) => [a] -> m (UM.MVector (PrimState m) a)
unsafeFromList = U.unsafeThaw . U.fromList

unsafeToList :: (Unbox a, PrimMonad m) => UM.MVector (PrimState m) a -> m [a]
unsafeToList = fmap U.toList . U.unsafeFreeze
