{-# LANGUAGE BangPatterns #-}
module Main where

import qualified Algorithms.MedianOfMedians  as MM
import           Algorithms.Partitions       as P
import qualified Algorithms.Quickselect      as Q
import qualified Algorithms.RandomizedMedian as RM
import           Algorithms.Sampling
import           Algorithms.Shuffle
import qualified Data.Vector.Unboxed         as U
import qualified Data.Vector.Unboxed.Mutable as UM

{- :!stack build --enable-executable-profiling --enable-library-profiling && stack exec selection-prof && hp2ps selection-prof.hp && ps2pdf selection-prof.ps && rm -f *.aux *ps *.hp
-}


main = do
  putStrLn "Starting"
  let l = [0..n] :: [Int]
      v = U.fromList l
  --rl <- shuffleM l
  --let rv = U.fromList l 
  mv <- U.thaw v
  --(fmap maximum . sequence) [UM.read mv i | i <- [0..n-1] ] >>= print
 -- printmedianR l
  -- printmedianRV v
  --printMMV v
  printMMVM mv
  --printPT mv (n`div`2)
  putStrLn "The end"
  where
    n :: Int
    n = 3*10^6
    printr x = putStrLn $ "Result: " ++ show x
    printpt (l, e, r) = print e
    printmedianR l = Q.medianR l >>= printr
    printmedianRV v = Q.medianRV v >>= printr
    printMM :: [Int] -> IO ()
    printMM = printr . MM.median
    printMMV :: U.Vector Int -> IO ()
    printMMV = printr . MM.medianV
    printMMVM x = MM.medianVM x >>= printr
    printPT x p = P.partition3VM x p >>= printpt
