{-
:!stack exec selection-bench -- --list
:!stack exec selection-bench -- "Median/Median Of Medians/"
:!stack exec selection-bench -- "Median/Quickselect/"
:!stack exec selection-bench -- "Median/"
:!stack exec selection-bench -- "Median/Median of Medians/list" "Median/Quickselect/vector rand" "Median/Sorting/built in" "Median/Median of Medians/mutable vector" "Median/Randomized Median/vector" "Median/Quickselect/list" --output median.html
-}

module Main where

import           Criterion
import           Criterion.Main
import           Criterion.Main.Options
import           Criterion.Measurement
import           Criterion.Types


import qualified Benchmarks.Median      as M
import           Benchmarks.Partitions
import           Benchmarks.Sampling

import qualified Algorithms.Partitions  as P
import qualified Data.Vector.Unboxed    as U

myConfig :: Config
myConfig = defaultConfig {
  timeLimit = 60
  }


main :: IO ()
main = defaultMainWith myConfig [
  partitionsE
  , samplingE
  , medianE
  ]
  where
    medianE = M.median (M.newMedianEnv (3*10^5) 1)
    samplingE = env (newSamplingEnv 99999) sampling
    partitionsE = env (newPartitionsEnv 99999 0.5) partitions
