% !TeX encoding = UTF-8
% !TeX spellcheck = en_GB
% A4 paper and 11pt font size
\documentclass[paper=a4, fontsize=11pt, abstract=on, parskip=half*]{scrartcl} 
%\documentclass[a4paper]{report}
\usepackage{lmodern}

% Use 8-bit encoding that has 256 glyphs
%\usepackage[T1]{fontenc} % for lualatex
\usepackage[utf8]{inputenc} % for pdflatex

% Math packages
\usepackage{amsfonts,amsthm,amsmath}

% Language/hyphenation
\usepackage[english]{babel}
%\selectlanguage{catalan} 
\usepackage{authblk}

\usepackage{csquotes}
\usepackage{textcomp}

%\usepackage{enumitem}
\usepackage{nameref}
\usepackage{numprint}

\usepackage{framed}

\usepackage{lipsum}
\usepackage{graphicx}
\usepackage{adjustbox}
\usepackage[scientific-notation=true]{siunitx}
\usepackage{verse}

\usepackage{subcaption}
\usepackage{nth}
\usepackage{fancyhdr}

\usepackage{hyperref}
\hypersetup{
  colorlinks,
  citecolor=black,
  filecolor=black,
  linkcolor=black,
  urlcolor=blue
}

%checkmark
\usepackage{pifont}

\usepackage{minted}

\usepackage[
    backend=biber,
    style=ieee,
    sortlocale=de_DE,
    natbib=true,
    url=false, 
    doi=true,
    eprint=false
]{biblatex}
\addbibresource{biblio.bib}
%\bibliography{biblio}

% inline lists
\usepackage{paralist}
\usepackage[x11names]{xcolor}


\pagestyle{fancy}
\fancyhf{}
\fancyhead[RE,LO]{\leftmark}
\fancyfoot[CE,CO]{\thepage}
 
\renewcommand{\headrulewidth}{1.3pt}
\renewcommand{\footrulewidth}{1pt}

% Comments
\newcommand{\niceremarkred}[3]{{\bf \textcolor{red}{\textsf{#1 #2: #3}}}}
\newcommand{\niceremarkmagenta}[3]{{\bf \textcolor{magenta}{\textsf{#1 #2: #3}}}}
\newcommand{\jan}[2][SAYS]{\niceremarkred{JAN}{#1}{#2}}
\newcommand{\david}[2][SAYS]{\niceremarkmagenta{DAVID}{#1}{#2}}

% Allo table recolocation
\usepackage{float}
\restylefloat{table}

\usepackage{setspace}
\setstretch{1.2}

%%%%%%%%%%%% 
%- MACROS -%
%%%%%%%%%%%%

\makeatletter
\newcommand*{\currentname}{\@currentlabelname}
\makeatother

\graphicspath{ {img/} }

% avoiding paragraph breaks
\widowpenalties 1 10000
\raggedbottom


\newenvironment{abstractpage}
  {\cleardoublepage\vspace*{\fill}\thispagestyle{empty}}
  {\vfill\cleardoublepage}
\renewenvironment{abstract}[1]
  {\bigskip\selectlanguage{#1}%
   \begin{center}\bfseries\abstractname\end{center}}
  {\par\bigskip}

% line spacing
\renewcommand{\baselinestretch}{1.2}
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal
% rule command with 1 argument of
% height

\newcommand{\cmark}{\ding{51}}%

\newcommand{\forceindent}{\leavevmode{\parindent=1em\indent}}

\newcommand*\widefbox[1]{\fbox{\hspace{2em}#1\hspace{2em}}}

\newcommand{\hask}[0]{\texttt{Haskell}}

\begin{document}

\begin{titlepage}
  \begin{center}


    \textsc{\LARGE Facultat d'informàtica de Barcelona, UPC}\\[1.5cm]
    
    \begin{figure}[H]
      \centering
    \end{figure}
    \textsc{\Large Randomized Algorithms}\\[0.5cm]

    % Title
    \horrule{0.5pt} \\[0.4cm]
    { \huge \bfseries Run-time analysis of median algorithms
      implemented in Haskell\\[0.4cm] }
  
    \horrule{2pt} \\[2.4cm]

    % Author and supervisor
    \noindent
    \begin{minipage}{0.4\textwidth}
      \begin{flushleft} \large
        \emph{Author:}\\
        Jan Mas Rovira
      \end{flushleft}
    \end{minipage}%
    \begin{minipage}{0.4\textwidth}
      \begin{flushright} \large
        \emph{Teacher:} \\
        Jordi Petit Silvestre \\
      \end{flushright}
    \end{minipage}

    \vfill
    
    % Bottom of the page
    {\large \today}

  \end{center}
\end{titlepage}


\newpage
\tableofcontents

\newpage

\section{Introduction}
The median of a set of elements is the element of the set such that if
we were to sort the set it would be in the middle. In this report I
analyze how different implementations in \hask{} of a median
algorithm behave in terms of time and memory efficiency as the input
grows.

\hask{} and its particularities play an strong role in this report, so
the conclusions drawn from the experiments are somewhat
restricted to the chosen language. The good part is that I can talk
about \hask{}.

The analyzed algorithms are: Sorting and
indexing, Quickselect, Randomized Quickselect, Median of medians and
Randomized median. For some of them I analyze more than one
implementation, using different data structures --- lists, vectors and
mutable vectors.

All the code from this project can be freely obtained at 
\url{https://bitbucket.org/janmasrovira/selection/src}. The important
part is at \texttt{src/Algorithms}.

\section{Running environment}
I ran all benchmarks on my laptop, which I --- with my parent's money--- bought during the summer
of 2011.
\begin{minted}[frame=lines,
framesep=2mm]{text}
system         SATELLITE P750 (PSAY3E-03S018CE)
  bus            PEQAA
  memory         64KiB BIOS
  processor      Intel(R) Core(TM) i7-2630QM CPU @ 2.00GHz
  memory         128KiB L1 cache
  memory         1MiB L2 cache
  memory         6MiB L3 cache
  memory         6GiB System Memory
  memory         4GiB SODIMM DDR3 Synchronous 1333 MHz (0.8 ns)
  memory         2GiB SODIMM DDR3 Synchronous 1333 MHz (0.8 ns)
\end{minted}

I used \verb|GHC-7.10.2| as the \hask{} compiler, using the following
flags: \verb|-O2 -0dph|.

\section{Packages}
I used some help from a few packages. I think it is worth mentioning three of them. 

\paragraph{\href{https://hackage.haskell.org/package/vector}{Vector}} From my personal experience in solving problems
that require the use of arrays in
the \verb|Jutge.org| environment, I learned
that the
\href{https://hackage.haskell.org/package/array-0.5.1.0}{\texttt{array}}
package that comes with \verb|GHC| by default sucks both
in terms of usability and efficiency. From reading blogs,
stackoverflow, and so on I knew that the de facto package
for arrays was
\href{https://hackage.haskell.org/package/vector}{\texttt{vector}}, so
I decided to give it a try. After using it myself, I can say that the
\texttt{vector} package is much better than the \texttt{array} package
in every sense\ldots well, except that it is not part of \texttt{GHC} by default.

\paragraph{\href{http://www.serpentine.com/criterion/}{Criterion}}
This is basically the only benchmarking library for \hask{} --- and it
is very good. Benchmarking \hask{} functions is not trivial due to
laziness. This package makes benchmarking both pure and impure
functions easy.

\paragraph{\href{https://github.com/timbod7/haskell-chart/wiki}{Charts}} 
I wanted to have an all-\hask{} project thus I needed a library for
doing some plots. This was the plotting package with more downloads so I
decided to go for it. I found the documentation to be very poor and
there wasn't many examples plus the community support is close to
non-existent. I have a lot of experience with \texttt{Python}'s \texttt{matplotlib} so
maybe it would have been a better alternative, however, I was too
stubborn to abandon my all-\hask{} utopia.


\section{Analysis}
Here I will comment on the algorithms. There is a section for each
algorithm. For the median of medians and quickselect algorithm there
are multiple implementations so they will be first compared and filtered
\textit{locally}. One implementation of each algorithm will pass to the final round and
fight against the other algorithms.

Each dot in a plot corresponds to the mean of 100 executions (10
executions for 10 different inputs).


\subsection{Naive median}
Not much to say, it uses the sorting function in the
\texttt{Data.List} module and then takes the $n/2$th element. Remember
that thanks to the laziness of \hask{}, the list is only sorted up to
the indexed element, so it is not as bad as in other
strict programming languages.


Well,
maybe there is a little bit more to say. I went to the
\href{https://hackage.haskell.org/package/base-4.8.1.0/docs/src/Data.OldList.html#sort}{source
  code} to take a peek at the sort implementation. I expected either
quicksort or introsort --- a mix of
quicksort and heapsort, used by \texttt{C++ std::sort} ---, however, to
my surprise, it was a variation of mergesort.

A traditional mergesort splits the original list into sublists of size
at most one and then it merges them pair by pair. The variation
\texttt{GHC} uses, first splits the list into already sorted sequences
--- reversing the decreasing sequences --- and then merges them just as the traditional mergesort.

To better understand it, let's take a look at an example. Consider the
string \texttt{"abctsrxyzba"}. The first step splits the string into \texttt{["abc",
  "rst", "xyz", "ab"]}. Then the sequences are merged by pairs:
\texttt{["abcrst", "abxyz"]} $\rightarrow$ \texttt{["aabbcrstxyz"]}.

According to the comments in the source code, until 2002
\texttt{GHC}'s sort was implemented with a quicksort. Benchmarks
showed that mergesort doubled the speed of the old quicksort
implementation, so it was replaced.

\subsection{3-way partitions}
A 3-way partition is a function that takes a list and a pivot, then
returns a list with the elements lesser than the pivot, a list with the
elements greater than the pivot and how many elements are equal to the pivot.

I implemented four partition algorithms.

\begin{itemize}
\item \textbf{List} Using \texttt{foldr}. Augmentative recursion\footnote{Augmentative recursion is the
    complementary of tail recursion.}.
\item \textbf{List'} Using
  \texttt{foldl'}. Strict tail recursion.
\item \textbf{Vector} Using the functions \texttt{partition},
  \texttt{filter} from the \texttt{vector} package.
\item \textbf{Mutable vector} An implementation of the fast 3-way
  partitioning by J. Bentley and D. McIlroy\cite{algo}. This
  implementation does less swaps than the Hoare partition.  
  \begin{figure}[h]
    \centering
    \includegraphics[width=0.55\textwidth]{part}
    \caption{Fast 3-way partitioning invariant diagram.}
\end{figure}
\end{itemize}

Let's have a look at the benchmark results.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{pt/charts1-time}
    \caption{Execution time comparison of partitioning algorithms.} 
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{pt/charts1-alloc}
    \caption{Allocated bytes comparison of partitioning algorithms.}
\end{figure}

The most surprising fact to me is that the mutable vector version is
the worst in terms of time efficiency. Initially I thought that
getting my hands dirty with indexes and swaps would lead to
better performance, but I was wrong. It also surprises me that it uses
as much memory as the list version since it does not use any
auxiliary list nor vector; all operations are in-place.

It is also worth mentioning why the \texttt{foldl'} version is so much
better than the \texttt{foldr} version. Well, as we already mentioned,
\texttt{foldl'} is tail recursive, and compilers can apply strong
optimizations. Great! let's always use \texttt{foldl'}! Hold on, it is
not always wise to use tail recursion. 

\begin{figure}[H]
  \centering
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{foldl}
    \caption{Application tree of \texttt{foldl}.}
  \end{subfigure}%
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{foldr}
    \caption{Application tree of \texttt{foldr}.}
  \end{subfigure}
  \label{fig:test}
\end{figure}

The thumb rule is that if you are certain that traversing the whole list will
be necessary to get a usable result, then use \texttt{foldl'}, else
use \texttt{foldr}. For example, to implement the sum of a list you
should use \texttt{foldl'} and for implementing a map you should
use \texttt{foldr}. Also, remember that for infinite lists
\texttt{foldl'} will never end.

In the partition case --- in the context of the median algorithm ---
it is best to use \texttt{foldl'} since we will
always have to traverse the whole list because we need to length of
the list of elements that are smaller/equal/greater than the pivot in
order to make the recursive call.

In conclusion, the strict fold on a list version goes to the final round.

\subsection{Median of medians}
\label{sec:mm}
The median of medians is a deterministic algorithm that guarantees
worst case $\mathcal{O}(n)$ running time. The only difference with
quickselect is how it chooses the pivot.

I implemented three variations of the algorithm using lists, vectors
and mutable vectors. Let's see how they behave.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{mm/chart1-time}
  \caption{Execution time comparison of median of medians implementations.} 
  \label{fig:mm-time}
\end{figure}


\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{mm/chart1-alloc}
  \caption{Allocated bytes comparison of median of medians implementations.} 
  \label{fig:mm-alloc}
\end{figure}

The results are quite similar to the ones in the partition
section. Mutable vectors lose again and lists stay on top. 

I was confused about why the version which uses mutable vectors was so
slow, so I compiled it and ran it using profiling flags. I thought
that the bottle neck would be the partitioning but it was not the
case. The function that took the most time, more than $50\%$ of the total, was
the sorting function. I programmed a simple in-place selection sort
but it was obviously very slow. Then I replaced it with an optimal
sort --- for arrays of length at most 5 --- from the
\href{https://hackage.haskell.org/package/vector-algorithms-0.7.0.1}{vector-algorithms}
package. Things got a little bit better but it was still very slow ---
$46\%$ of the total time. I tried two more things: using unboxed
arrays and forcing strictness. The former gave a slight boost while the
latter did nothing\footnote{Later on I learned that unboxed arrays are
already strict.}. Figures \ref{fig:mm-time} and \ref{fig:mm-alloc}
display the performance after all the mentioned optimizations.

I it is clear that either I am missing some important optimizations
on my code or that mutable arrays are not meant
for \hask{}. Either way I am somewhat disappointed and hopefully I
will clarify this issue someday in the future.

\subsection{Quickselect}

After the mutable vector fiasco in \nameref{sec:mm} section I gave up
on mutability. I implemented four versions of the quickselect
algorithm.

\begin{itemize}
\item \textbf{Deterministic using lists}
\item \textbf{Deterministic using vectors}
\item \textbf{Randomized using lists}
\item \textbf{Randomized using vectors} 
\end{itemize}

I am pretty sure that at this point everybody knows which is the best
version, but let's sate our empiricist appetite.

For generating the input I programmed a slight variation of
the classic shuffle algorithm. Before making a swap, a coin with
probability $p$ is flipped. If we get tails then we swap as normal,
else we do nothing. This shuffle is applied to the list
\texttt{[1..n]}.

I tested the four versions on different values of $p$, let's have a
look at the results.

\begin{figure}[H]
  \centering
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=.95\linewidth]{qs/charts1_p0-0}
    \caption{$p = 0$ (sorted).}
  \end{subfigure}%
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=.95\linewidth]{qs/charts1_p0-25}
    \caption{$p = 0.25$.}
  \end{subfigure}
\\[2ex]
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=.95\linewidth]{qs/charts1_p0-5}
    \caption{$p = 0.5$.}
  \end{subfigure}%
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=.95\linewidth]{qs/charts1_p1-0}
    \caption{$p = 1$ (completely shuffled).}
  \end{subfigure}
\end{figure}


Everything went as expected. For sorted inputs, the deterministic
algorithms do extremely bad --- $\mathcal{O}(n^2)$ --- so they are
discarded. As for the randomized versions, the one that uses a list is
also discarded because picking a random element within a list has cost
$\mathcal{O}(n)$ in \hask{}.

\subsection{Randomized median}
This algorithm has the particularity that it may fail. In class we
found that with high probability it will not fail as $n$ grows. Since
the probability of failing does not depends on the implementation,
there is no need to go into much details. In practice, with list of
100 elements, which is quite short, it takes about 3000 executions to fail.
For a list of 300, it takes about 45000. For a list of 5000\dots I got
bored of waiting.

The function i programmed reruns the algorithm until it succeeds. With
high probability will succeed on the first run.

\subsection{And the winner is\dots}

Remember which candidates arrived at the finals.

\begin{itemize}
\item Naive median (sorting).
\item Median of medians using lists.
\item Randomized quickselect using vectors.
\item Randomized median.
\end{itemize}

The time to unveil the champion has finally come.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{med/chart1-time}
    \caption{Execution time comparison of the finalist implementations.}
\end{figure}

As for execution time, \textbf{randomized quickselect} is the best out of the four
finalists. However, \textbf{randomized median} is very close to it. So
there is no clear-cut winner. Nonetheless, it is clear that the naive median
and the median of medians algorithms do not perform as good as their
competitors. For the naive median it was quite expected, however, for
the median of medians, this might come as a surprise, since,
theoretically, it is optimal. As we know, asymptotic notation is
sometimes too general because it ignores constants so it is important
to make benchmarks and clarify which algorithms perform best in practice.

In this beautiful \href{./files/median.html}{report} there is a
more statistically in-depth analysis of the running time.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{med/chart1-alloc}
    \caption{Allocated bytes comparison of the finalist implementations.} 
\end{figure}

With regards to memory allocation, the \textbf{randomized median}
clearly outclasses its adversaries. It is also worth mentioning that the
median of medians implementation needs a lot more memory than the others.

The jury concludes that the winner is the \textbf{randomized median}
algorithm since it uses very little memory and runs fast --- only slightly
surpassed by randomized quickselect.

\section{Avoiding the IO monad}

\paragraph{Disclaimer} This section is a little bit freaky and has nothing to do with
randomized algorithms.

Some of the algorithms --- or a parts of them --- that we analyze rely
on randomness and mutability. For instance, shuffling a list
efficiently relies on
both. Those are concepts that do not fit very well with a purely
functional language. Fortunately, monads come to the rescue!

When I first implemented the shuffling algorithm, I had a moment of
weakness and I succumbed to the IO monad --- think of it as the devil
of \hask{}. The type signature of the first implementation was like this.

\begin{minted}[frame=lines,
framesep=2mm]{haskell}
shuffle :: [a] -> IO [a]
\end{minted}

\hask{} has a lot of monads, and most of them are pure, and most
important, you can get out whenever you like. For example, to get out
of the Maybe monad you can use the function \texttt{fromJust}. However, as you may
know, once you enter the realms of the IO monad\ldots there is no way
back\footnote{unless you are mad enough to use unsafePerformIO from
  the System.IO.Unsafe module.}. Before continuing, let's read this
excerpt from the \textit{The IO} poem --- 
\href{https://mail.haskell.org/pipermail/haskell-cafe/2006-December/020005.html}{full poem}.

\begin{minipage}{\textwidth}
\poemtitle*{The IO}
\settowidth{\versewidth}{He has the boy type safe in his arm ddddss}
\begin{verse}[\versewidth]
Who rides so late through the bits and the bytes?\\
It's Haskell with his child Hank;\\
He has the boy type safe in his arm,\\
He holds him pure, he holds him warm.

``My son, what makes you hide your face in fear?''\\
\vin Father, don't you see the IO?\\
\vin The IO with randomRIO?\\
``My son, it's a wisp of the outside world.''
\end{verse}
\end{minipage}
\vspace{1cm}



Breathtaking\dots If you know how the original poem --- Der Erlkönig --- ends, you
should be scared of the IO monad. 

The night after coding the shuffling
algorithm I could not rest well so
I woke up and I went to the internet looking for help.

After deep research I encountered some allies. 

\begin{itemize}
\item \textbf{Primitive monad} Is the class of monads which allow
  primitive state-transformer actions.
\item \textbf{ST monad} monad which allows primitive state-transformer
  actions within a single strict thread. Just as the IO monad, it is
  an instance of Primitive monad, however, it is pure and
  escapable.
\item \textbf{monad Random} Is the class of monads which allow random
  number generation. There are two ways of
  running a computation within the Random monad. One is to run it on the IO
  monad, since the IO monad is an instance of the Random monad. The
  other way is giving an initial seed. Note that by giving a seed, the
  computation becomes pure.
\end{itemize}

So I had the tools I needed, but one problem was still
unsettled. How could I combine the Primitive monad and the Random
monad classes in a single one in order
to have both mutability and random numbers? After some more research I
came across with monad transformers, which they do exactly this\cite{o2008real}.

This piece of code, which I borrowed from \href{https://gist.github.com/sdroege/6209e97b4dfc9791033d}{Sebastian Dröge github}, is
what let us combine the mentioned monads. 

\begin{minted}[frame=lines,
framesep=2mm]{haskell}
{-# LANGUAGE TypeFamilies #-}
instance PrimMonad m => PrimMonad (RandT g m) where
    type PrimState (RandT g m) = PrimState m
    primitive = lift . primitive
\end{minted}

Since the IO monad is an instance of both classes, we can now shuffle
a list in two different ways: pure and
impure. The pure way is only possible due to the above instance. Let's see an example:
\begin{minted}[frame=lines,
framesep=2mm]{haskell}
shuffle :: (PrimMonad m, MonadRandom m) => [a] -> m [a]

pureExample :: [Int] -> [Int]
pureExample l = runST $ evalRandT (shuffle l) (mkStdGen 123)

ioExample :: [Int] -> IO [Int]
ioExample l = shuffle l
\end{minted}

In this project, every function that requires mutability or
randomness follows this approach.

Getting to understand and make all this work was a lot of time but I
think this was the most beautiful thing I learned doing this project.

\newpage
\nocite{*}
\printbibliography

\end{document}
